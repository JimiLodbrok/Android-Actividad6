package com.example.jaime.actividad3.utils.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.instance.DataHolder;
import com.example.jaime.actividad3.utils.cells.CeldaJugador;

public class ListAdapter extends RecyclerView.Adapter<CeldaJugador> {
    private Context context;

    public ListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CeldaJugador onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_prototype,null);
        CeldaJugador celda = new CeldaJugador(view);
        return celda;
    }

    @Override
    public void onBindViewHolder(CeldaJugador celda, int position) {
        Log.v("ActivityList","Siiii lleggaaa------------------------------------->" + DataHolder.getInstance().getJugadores().get(position).getNombre());
        celda.setNombreJugador(DataHolder.getInstance().getJugadores().get(position).getNombre());
        celda.setNombreEquipo(DataHolder.getInstance().getJugadores().get(position).getEquipo());
        celda.setAlturaJugador(String.valueOf(DataHolder.getInstance().getJugadores().get(position).getAltura()) + " cm");
       // celda.setImagenJugador(DataHolder.getInstance().getJugadores().get(position).getImage());
        if(DataHolder.getInstance().getJugadores().get(position).getUrlImage() != null)
            Glide.with(context).load(DataHolder.getInstance().getJugadores().get(position).getUrlImage()).into(celda.getImagenJugador());
        else
            Glide.with(context).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(celda.getImagenJugador());

    }

    @Override
    public int getItemCount() {
        return DataHolder.getInstance().getJugadores().size();
    }
}
