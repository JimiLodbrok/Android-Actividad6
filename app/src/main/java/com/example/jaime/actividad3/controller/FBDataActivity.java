package com.example.jaime.actividad3.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.bumptech.glide.Glide;
import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.firebase.FirebaseAdmin;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;
import com.example.mylibrary.fragments.Facebook_TwitterFragment;
import com.facebook.login.LoginManager;
import com.twitter.sdk.android.core.Twitter;

public class FBDataActivity extends AppCompatActivity implements EventsListener {
    private Facebook_TwitterFragment facebookFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fbdata);

        Bundle inBundle = getIntent().getExtras();

        facebookFragment = (Facebook_TwitterFragment) getSupportFragmentManager().findFragmentById(R.id.fbFragment);
        facebookFragment.getNombre().setText(inBundle.getString("name") + " " + inBundle.getString("surname"));
        facebookFragment.getAmigos().setText(inBundle.getString("gender"));
        facebookFragment.getAmigoss().setText(inBundle.getString("birthday"));
        if(inBundle.getString("imageUrl")!= null)
            Glide.with(this).load(inBundle.getString("imageUrl")).into(facebookFragment.getImagenPerfil());
        else
            Glide.with(this).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(facebookFragment.getImagenPerfil());
        EventsAdmin.getInstance().addListener(this);
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.show(facebookFragment);
        transition.commit();
    }

    @Override
    public void onClickScren(View view) {
        if (view.getId() == R.id.btnCerrar) {
            LoginManager.getInstance().logOut();
            FirebaseAdmin.getInstance().signOut();
            Intent secondActivity = new Intent(this, ActivityLogin.class);
            startActivity(secondActivity);
            finish();
        }
    }
}
