package com.example.jaime.actividad3.model.persistence;

import com.google.android.gms.maps.model.Marker;
import com.google.firebase.database.Exclude;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jaime on 15/01/2018.
 */

public class Perfil {
    private String nombre,uid;
    private Double lat,lon;
    //private Marker marker;

    public Perfil() {

    }

    public Perfil(String nombre, String uid, Double lat, Double lon) {
        this.nombre = nombre;
        this.uid = uid;
        this.lat = lat;
        this.lon = lon;
    }

    @Exclude
    public Map<String,Object> toMap() {
        Map<String,Object> resultado = new HashMap<>();
        resultado.put("lat",lat);
        resultado.put("lon",lon);
        resultado.put("nombre",nombre);

        return resultado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
/*
    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
*/

}
