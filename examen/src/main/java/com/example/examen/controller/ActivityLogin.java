package com.example.examen.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import com.example.examen.R;
import com.example.examen.model.firebase.FirebaseAdmin;
import com.example.examen.model.firebase.FirebaseLoginListener;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;
import com.example.mylibrary.fragments.LoginFragment;
import com.example.mylibrary.fragments.RegisterFragment;

public class ActivityLogin extends AppCompatActivity implements EventsListener, FirebaseLoginListener {
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*
        Referencio los fragments de mylibrary que voy a utilizar
         */
        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.registerFragment);
        //Seteo el listener y el activity para informar al FirebaseAdmin
        FirebaseAdmin.getInstance().setLoginListener(this);
        FirebaseAdmin.getInstance().setActivity(this);

        //Seteo el listener para decir que esta es la activity que está escuchando al eventsAdmin
        EventsAdmin.getInstance().addListener(this);

        //muestro el login Fragment
        onReturnClicker();
    }

    /*
        Método al que se llamará cuando se pulse el boton de Sign In
        Comprueba que los campos no están vacíos y llama al método signIn() del FirebaseAdmin.
        Sino muestra un mensaje de error
     */
    public void onLoginClicked() {
        if (!loginFragment.getEdtxt_User().getText().toString().equals("") && !loginFragment.getEdtxt_Password().getText().toString().equals(""))
            FirebaseAdmin.getInstance().signIn(loginFragment.getEdtxt_User().getText().toString(), loginFragment.getEdtxt_Password().getText().toString());
        else
            Toast.makeText(this, "Fill the sign in fields",
                    Toast.LENGTH_SHORT).show();

    }

    /*
        Método al que se llamará cuando se pulse el boton de Sign Up(en el registrerFragment)
        Comprueba que los campos no están vacíos, las contraseñas coincidan y llama
         al método createAccount() del FirebaseAdmin.
        Sino muestra un mensaje de error
     */
    public void onNewRegisterClicker() {
        if (!registerFragment.getEdtxt_NewUser().getText().toString().equals("") && !registerFragment.getEdtxt_NewPassword().getText().toString().equals(""))
            if (registerFragment.getEdtxt_NewPassword().getText().toString().equals(registerFragment.getEdtxt_NewPassword2().getText().toString())) {
                loginFragment.getEdtxt_User().setText(registerFragment.getEdtxt_NewUser().getText().toString());
                FirebaseAdmin.getInstance().createAccount(registerFragment.getEdtxt_NewUser().getText().toString(), registerFragment.getEdtxt_NewPassword().getText().toString());
            } else
                Toast.makeText(this, "The passwords have to match",
                        Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Pleas fill all the fields.",
                    Toast.LENGTH_SHORT).show();
    }

    /*
        Método al que se llamará cuando se pulse el boton de Register(en el loginFragment)
        Hace una transición , oculta el fragmentLogin y muestra el fragmentRgister
        Limpia los edit texts del login fragment
     */
    public void onRegisterClicked() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(loginFragment);
        transition.show(registerFragment);
        transition.commit();
        clearLoginFragment();
    }

    /*
        Método al que se llamará cuando se pulse el boton de Back(en el registerFragment)
        Hace una transición , oculta el fragmentRgister y muestra el fragmentLogin
        Limpia los edit texts del register fragment
     */
    public void onReturnClicker() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(registerFragment);
        transition.show(loginFragment);
        transition.commit();
        clearRegisterFragment();
    }

    /*
        Método que limpia los edit texts
     */
    public void clearLoginFragment() {
        loginFragment.getEdtxt_User().setText("");
        loginFragment.getEdtxt_Password().setText("");
    }

    /*
        Método que limpia los edit texts
     */
    public void clearRegisterFragment() {
        registerFragment.getEdtxt_NewUser().setText("");
        registerFragment.getEdtxt_NewPassword().setText("");
        registerFragment.getEdtxt_NewPassword2().setText("");
    }

    /*
        Método que llama el eventsAdmin cuando detecta un clic
        Comprueba con los if, desde qué boton(view) se ha clicado, para poder realizar diferentes acciones
        dependiendo del boton.
     */
    @Override
    public void onClickScren(View view) {
        if (view.getId() == R.id.btnLogin) {
            onLoginClicked();
        } else if (view.getId() == R.id.btnRegister)
            onRegisterClicked();
        else if (view.getId() == R.id.btnNRegister) {
            onNewRegisterClicker();
        } else if (view.getId() == R.id.btnVolver)
            onReturnClicker();
    }

    /*
       Método que se ejecuta cuando se logea correctamente en el FirebaseAdmin
       Hace la transición
     */
    @Override
    public void userSignedIn() {
        Intent secondActivity = new Intent(this, ActivityList.class);
        startActivity(secondActivity);
    }

    /*
       Método que se ejecuta cuando se crea correctamente el usuario en el FirebaseAdmin
       Vuelve al Fragment Login
     */
    @Override
    public void userCreated() {
        onReturnClicker();
        clearRegisterFragment();
    }
}
