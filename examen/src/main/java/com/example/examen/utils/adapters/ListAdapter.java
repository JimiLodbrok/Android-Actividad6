package com.example.examen.utils.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.examen.R;
import com.example.examen.model.instance.DataHolder;
import com.example.examen.utils.cells.CeldaNoticia;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<CeldaNoticia>{
    private Context context;
    private ListaAdapterListener listener;

    public ListAdapter(Context context) {
        this.context = context;
    }

    public void setListener(ListaAdapterListener listener) {
        this.listener = listener;
    }
    /*
        Asigna el adapter a la celda prototipo para que la rellene con los datos que reciba
     */
    @Override
    public CeldaNoticia onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_prototype,null);
        CeldaNoticia celda = new CeldaNoticia(view);
        return celda;
    }

    /*
        Se rellena la celda en la posicion indicada con los métodos del array estatico ya actualizados.
        En el if, permito que si no tengo una imagen asignada a la noticia, se ponga una imagen por defecto
     */
    @Override
    public void onBindViewHolder(CeldaNoticia celda, int position) {
        celda.setTitulo(DataHolder.getInstance().getNoticias().get(position).getTitulo());
        if(DataHolder.getInstance().getNoticias().get(position).getUrlImage() != null)
            Glide.with(context).load(DataHolder.getInstance().getNoticias().get(position).getUrlImage()).into(celda.getImagen());
        else
            Glide.with(context).load("https://image.jimcdn.com/app/cms/image/transf/none/path/s8f19d879eab85ed4/image/i41b8a15b1686c9f3/version/1486992412/image.png").into(celda.getImagen());
        celda.setListener(this.listener);
    }

    /*
        Método que devuelve el número de celdas a pintar en el recyclerView(le pasamos el tamaño del array estastico)
     */
    @Override
    public int getItemCount() {
        return DataHolder.getInstance().getNoticias().size();
    }
}
