package com.example.examen.utils.adapters;

import com.example.examen.utils.cells.CeldaNoticia;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public interface ListaAdapterListener {
    void listaClicked(CeldaNoticia celda);
}
