package com.example.examen.model.firebase;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.examen.model.instance.DataHolder;
import com.example.examen.model.persistence.Noticia;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public class FirebaseAdmin {
    /*
     Creo una referencia estática y los atributos necesarios para usar Firebase
     */
    private static final FirebaseAdmin instance = new FirebaseAdmin();
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private Activity activity;
    private FirebaseUser user;
    private FirebaseLoginListener listenerLogin;
    private FirebaseDataListener listenerData;

    public FirebaseAdmin() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference();
    }

    /*
        Método para acceder a la referencia estática de FirebaseAdmin
     */
    public static FirebaseAdmin getInstance() {
        return instance;
    }

    /*
        Método para setear el listener del login
     */
    public void setLoginListener(FirebaseLoginListener listenerLogin) {
        this.listenerLogin = listenerLogin;
    }

    /*
        Método para setear el listener del los datos de realtimeDatabase
     */
    public void setDataListener(FirebaseDataListener listenerData) {
        this.listenerData = listenerData;
    }

    /*
        Método para setear el activity en el que voy a mostrar mensajes por medio de Toast
     */
    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    /*
        Metodo para crear usuarios en el Firebase.
        Le paso un email y un pass por parámetros y crea el usuario
        Si no ha habido fallos, ira por la rama del if->Accederá al metodo del listener...userCreated()
        lo que hará que cambie de fragment el activity al fragment login.
     */
    public void createAccount(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(activity, "User created.",
                                    Toast.LENGTH_SHORT).show();
                            listenerLogin.userCreated();
                        } else {
                            Toast.makeText(activity, "No se pudo crear el usuario(la contraseña debe ser de almenos 6 digitos).",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    /*
        Método para logearse en la aplicación
        Le paso un email y pass que introducirá el usuario en los textfields(recogidos en el fragmentlogin)
        Si existe el usuario en la base de datos de Firebase, pasará por la rama if->Accederá al metodo
        del listener....userSignedIn() lo que hará que se cree un intent y se muestre la segunda activity.
     */
    public void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            user = mAuth.getCurrentUser();
                            listenerLogin.userSignedIn();
                        } else {
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /*
        Metodo para descargarse los datos de la rama que estás escuchando y le pasas por parametro.
        Descarga la informacion y la castea a un ArrayList y lo mete en el Array estatico.
        Después se llama al método del listener dataLoaded() en este caso es el segundo activity
        y actualiza el recyclerview con la informacion del Array estatico actualizado

     */
    public void loadData(String rama) {
        DatabaseReference databaseRef = mDatabase.child(rama);
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<ArrayList<Noticia>> indicator = new GenericTypeIndicator<ArrayList<Noticia>>(){};
                DataHolder.getInstance().setNoticias(dataSnapshot.getValue(indicator));
                listenerData.dataLoaded();
            }
            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public FirebaseUser getCurrentUser() {
        return user;
    }

}
