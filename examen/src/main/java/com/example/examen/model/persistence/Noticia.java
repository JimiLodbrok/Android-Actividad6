package com.example.examen.model.persistence;

/**
 * Created by jaime.castan on 19/12/2017.
 */
/*
    Clase que contiene las propiedades de las noticias
 */
public class Noticia {
    private String titulo;
    private String urlImage;

    public Noticia() {

    }

    public Noticia(String titulo, String urlImage) {
        this.titulo = titulo;
        this.urlImage = urlImage;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String nombre) {
        this.titulo = nombre;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
