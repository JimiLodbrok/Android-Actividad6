package com.example.examen.model.instance;

import com.example.examen.model.persistence.Noticia;

import java.util.ArrayList;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public class DataHolder {
    /*
        DataHolder estatico para acceder al array que contiene
        toda la informacion de la base de datos
     */
    private static DataHolder instance = new DataHolder();
    private ArrayList<Noticia> noticias;

    public DataHolder() {
        noticias = new ArrayList();
    }

    public static DataHolder getInstance() {
        return instance;
    }

    public ArrayList<Noticia> getNoticias() {
        return noticias;
    }

    public void setNoticias(ArrayList<Noticia> noticias) {
        this.noticias = noticias;
    }
}
